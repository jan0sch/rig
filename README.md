# Generate a random identity.

This is a small tool which will generate a random identity.

Currently it will only generate names common in germany with a matching postal
code and city name.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details how to to 
contribute to the project.

## License

This program is distributed under 3-Clause BSD license. See the file 
[LICENSE](LICENSE) for details.

## Installation

To simply install the binary just issue the
`cargo install random-identity-generator` command which will place the
executable `rig` into your `$PATH`.

### From source code

Either create a binary with `cargo build --release` and copy it to a place
of your liking or use the `cargo install --path .` command which will 
install it into `$HOME/.cargo/bin`.

## Examples

```txt
% rig -h
Random Identity Generator 1.0.0
Generate a random identity.

USAGE:
    rig [country]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <country>    Pick sources for a specific country.
% rig
Eva Schmitt, 12165 Berlin
% rig
Noah König, 6556 Ringleben
```

